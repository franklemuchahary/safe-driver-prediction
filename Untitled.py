
# coding: utf-8

# In[28]:

import pandas as pd
import numpy as np
from collections import Counter
import missingno as mno

import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')

import plotly.offline as py
py.init_notebook_mode(connected=True)
import plotly.graph_objs as go
import plotly.tools as tls
import warnings
warnings.filterwarnings('ignore')

from sklearn.feature_selection import chi2


# #### Import Train Data

# In[3]:

train_data = pd.read_csv('train.csv')


# In[4]:

print("Rows", train_data.shape[0])
print("Cols", train_data.shape[1])


# #### Checking distribution of target variable to see if balanced or unbalanced

# In[5]:

data = [go.Bar(
    x = [train_data['target'].value_counts().index[0], train_data['target'].value_counts().index[1]],
    y = train_data['target'].value_counts(),
    marker=dict(
        color='rgb(158,202,225)',
        line=dict(
            color='rgb(0,0,0)',
            width=2.5,
        )
    ),
    opacity=0.6
)]

layout = go.Layout(
    title='Distribution of Target Values (Highly Unbalanced)',
    xaxis=dict(
        title='Target Value (0/1)'
    ),
    yaxis=dict(
        title='Count'
    )
)

fig = go.Figure(data=data, layout=layout)

py.iplot(fig, filename='basic-bar')


# #### Check for missing data

# In[6]:

train_data.isnull().any().any()


# In[7]:

#missing values have been stored as -1 
temp = train_data
temp = temp.replace(-1, np.NaN)


# In[8]:

#plot to see the missing values using misingno library
mno.matrix(df=temp.iloc[:, :30], color=(0.10, 0.4, 0.1))


# In[9]:

#plot to see the missing values using missingno library
mno.matrix(df=temp.iloc[:, 30:], color=(0.10, 0.4, 0.1))


# In[25]:

features_with_many_missing = ["ps_ind_05_cat", "ps_reg_03", "ps_car_03_cat", "ps_car_05_cat", "ps_car_07_cat", 
                              "ps_car_09_cat", "ps_car_14"]


# #### Correlation Plot 
# Useful measure to see if there are any linear relationships between the different variables

# In[99]:

#segregate the dataset into floats and ints
dtypes = Counter(train_data.dtypes).keys()

temp_float = train_data.select_dtypes(include=['float64'])
temp_int = train_data.select_dtypes(include=['int64'])


# In[21]:

plt.figure(figsize=(20,15))
plt.title("Pearson Correlation Coefficient Matrix", y=1.05, size=15)
sns.heatmap(train_data.corr(), cmap=plt.cm.plasma, square=True)


# In[106]:

plt.figure(figsize=(20,15))
plt.title("Pearson Correlation Coefficient Matrix", y=1.05, size=15)
sns.heatmap(temp_float.corr(), cmap=plt.cm.inferno, square=True)


# #### Chi Sqared test for independence 
# for categorical variables to see if there is a relationship with the target values

# In[35]:

temp_int.describe()


# In[49]:

temp_int_chi = temp_int.replace(-1, 111111)
chi2_results = chi2(temp_int_chi.iloc[:, 2:].values, temp['target'].values)

features_p_values_dict = dict()
features_p_values_sigf = dict()
for i,j in zip(temp_int_chi.iloc[:, 2:].columns.values, chi2_results[1]):
    features_p_values_dict[i] = j
    if j<0.05:
        features_p_values_sigf[i] = j


# In[117]:

data = [go.Bar(
    x = list(features_p_values_dict.keys()),
    y = list(features_p_values_dict.values()),
    marker=dict(
        color='rgb(108,202,250)',
        line=dict(
            color='rgb(0,0,0)',
            width=2.5,
        )
    ),
    opacity=0.6
)]

layout = {
    'title':'p-values Chi Squared Test for Independence for Categorical Variables',
    'yaxis':dict(
        title='p-value',
    ),
    'barmode':'relative'
}



py.iplot({'data':data, 'layout':layout}, filename='barmode-relative')

